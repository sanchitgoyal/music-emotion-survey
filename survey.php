<?php
    session_start();

    //These variable values need to be changed by you before deploying
    $password = "Ph935053882!";

    if(isset($_POST['form-action'])){

        if($_POST['form-action'] == "Save"){
            if($_POST["music-clip"]==""){
                //Connecting to your database
                $conn = mysql_connect($_SESSION['hostname'], $_SESSION['username'], $password) OR DIE ("Unable to 
                connect to database! Please try again later.");
                mysql_select_db($_SESSION['dbname']);

                $tempoFrom = $_POST["tempo-from"];
                $tempoTo = $_POST["tempo-to"];
                $pitchMin = $_POST["pitch-from"];
                $pitchMax = $_POST["pitch-to"];
                $angryMax = $_POST["emotion-angry-to"];
                $angryMin = $_POST["emotion-angry-from"];
                $sadMax = $_POST["emotion-sad-to"];
                $sadMin = $_POST["emotion-sad-from"];
                $peacefulMax = $_POST["emotion-peaceful-to"];
                $peacefulMin = $_POST["emotion-peaceful-from"];
                $happyMax = $_POST["emotion-happy-to"];
                $happyMin = $_POST["emotion-happy-from"];
                $musicClip = $_POST["music-clip"];

                $query = "INSERT INTO survey_data (surveyorID,tempoMin,tempoMax,pitchMin,pitchMax,angryMin,angryMax,sadMin,sadMax,peacefulMin,peacefulMax,happyMin,happyMax,musicClip) 
                            VALUES($_SESSION[surveyorID],$tempoFrom,$tempoTo,$pitchMin,$pitchMax,$angryMin,$angryMax,$sadMin,$sadMax,$peacefulMin,$peacefulMax,$happyMin,$happyMax,'$musicClip')";
                $result = mysql_query($query,$conn);

                if (! $result) {
                    die('Could not enter data: ' . mysql_error());
                }

                $success = "true";
            }
            else{
                $success = "no-music-clip";
            }

        }
        else{
            session_destroy();
            header('Location:index.php');
        }
    }    
?>

<!DOCTYPE html>
<html>	
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Music Element Fuzzy Value Survey</title>    

        <!-- JQuery UI CSS -->
        <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.1/themes/base/jquery-ui.css" />

        <!-- Foundation CSS -->
        <link rel="stylesheet" href="plugins/foundation/stylesheets/foundation.css" />  
        <link rel="stylesheet" href="plugins/foundation/stylesheets/app.css" />         

        <!-- JQuery -->
        <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
        <script src="http://code.jquery.com/ui/1.10.1/jquery-ui.js"></script>

        <!-- Foundation Configuration JQuery -->
        <script src="plugins/foundation/javascripts/modernizr.foundation.js"></script>    

        <script type="text/javascript">
            $(document).ready(function(){
                /*************************
                *       Initializers
                ***************************/
                $( "#tempo-slider-range" ).slider({
                    range:true,
                    min: 0,
                    max: 200,
                    values:[95,100],
                    slide: function(event,ui){
                        $('#tempoFrom').val(ui.values[0]);
                        $('#tempoTo').val(ui.values[1]);
                    }
                });

                $( "#pitch-slider-range" ).slider({
                    range:true,
                    min: 0,
                    max: 20000,
                    values:[9995,10000],
                    slide: function(event,ui){
                        $('#pitch-from').val(ui.values[0]);
                        $('#pitch-to').val(ui.values[1]);
                    }
                });

                $( "#angry-slider-range" ).slider({
                    range:true,
                    min: 0,
                    max: 10,
                    values:[5,5],
                    slide: function(event,ui){
                        $('#emotion-angry-from').val(ui.values[0]);
                        $('#emotion-angry-to').val(ui.values[1]);
                    }
                });

                $( "#sad-slider-range" ).slider({
                    range:true,
                    min: 0,
                    max: 10,
                    values:[5,5],
                    slide: function(event,ui){
                        $('#emotion-sad-from').val(ui.values[0]);
                        $('#emotion-sad-to').val(ui.values[1]);
                    }
                });

                $( "#peaceful-slider-range" ).slider({
                    range:true,
                    min: 0,
                    max: 10,
                    values:[5,5],
                    slide: function(event,ui){
                        $('#emotion-peaceful-from').val(ui.values[0]);
                        $('#emotion-peaceful-to').val(ui.values[1]);
                    }
                });

                $( "#happy-slider-range" ).slider({
                    range:true,
                    min: 0,
                    max: 10,
                    values:[5,5],
                    slide: function(event,ui){
                        $('#emotion-happy-from').val(ui.values[0]);
                        $('#emotion-happy-to').val(ui.values[1]);
                    }
                });  
                /****************************
                *       Action Handlers
                *****************************/

                // Clip Select List
                $('#music-clip').bind({
					dblclick: function(){
						$('#mediaplayer param[name=filename]').attr('value','music/'+$(this).val()).change();
						$('#mediaplayer embed').attr('src','music/'+$(this).val()).change();
					}
				});

                //Save Button
                $('#saveAction').bind({
                    click:function(){

                    }
                })

                <?php
                    if($success=="true"){
                        echo "alert('Response Recorded');";
                    }
                    else if($success="no-music-clip"){
                        echo "alert('Please select a music clip!');";
                    }
                ?>

            });            
		</script>

    </head>
    <body>
        <div class="row">
            <div class="two columns"></div>
            <div class="eight columns" style="text-align:center"><h3>Music Element Fuzzy Value Survey</h3></div>
            <div class="two columns"></div>
        </div>

        <form action="" method="post">

        <div class="row">
            <div class="seven columns">
                <div class="row">  
                    <object id="mediaplayer" classid="clsid:22d6f312-b0f6-11d0-94ab-0080c74c7e95" 
                            codebase="http://activex.microsoft.com/activex/controls/mplayer/en/nsmp2inf.cab#version=5,1,52,701" 
                            standby="loading microsoft windows media player components..." type="application/x-oleobject" >
                        <param name="filename" value="test.mp3">
                        <param name="animationatstart" value="true">
                        <param name="transparentatstart" value="true">
                        <param name="autostart" value="false">
                        <param name="showcontrols" value="true">
                        <param name="ShowStatusBar" value="true">
                        <param name="windowlessvideo" value="true">
                        <embed src="test.mp3" autostart="true" showcontrols="true" showstatusbar="1" bgcolor="black" width="100%" >
                    </object>

                </div>

                <br />

                <div class="row">                    
                    <div class="two columns"><h5>Tempo</h5></div>
                    <div class="six columns">
                        <br />
                        <div class="row">
                            <div class="two columns">0</div>
                            <div id="tempo-slider-range" class="eight columns"></div>
                            <div class="two columns">200</div>
                        </div>                            
                    </div>
                    <div class="four columns row">     
                        <div class="row">
                            <div class="six columns"><input type="text" readonly="readonly" id="tempoFrom" name="tempo-from" /></div> 
                            <div class="six columns"><input type="text" readonly="readonly" id="tempoTo" name="tempo-to" /></div>
                        </div>                            
                    </div>    
                </div>    

                <div class="row">                    
                    <div class="two columns"><h5>Pitch</h5></div>
                    <div class="six columns">
                        <br />
                        <div class="row">
                            <div class="two columns">0</div>
                            <div id="pitch-slider-range" class="eight columns"></div>
                            <div class="two columns">20000</div>
                        </div>                            
                    </div>
                    <div class="four columns row">     
                        <div class="row">
                            <div class="six columns"><input type="text" readonly="readonly" id="pitch-from" name="pitch-from" /></div> 
                            <div class="six columns"><input type="text" readonly="readonly" id="pitch-to" name="pitch-to" /></div>
                        </div>                            
                    </div>    
                </div>    

                <div class="row">                    
                    <div class="two columns"><h5>Angry</h5></div>
                    <div class="six columns">
                        <br />
                        <div class="row">
                            <div class="two columns">0</div>
                            <div id="angry-slider-range" class="eight columns"></div>
                            <div class="two columns">10</div>
                        </div>                            
                    </div>
                    <div class="four columns row">     
                        <div class="row">
                            <div class="six columns"><input type="text" readonly="readonly" id="emotion-angry-from" name="emotion-angry-from" /></div> 
                            <div class="six columns"><input type="text" readonly="readonly" id="emotion-angry-to" name="emotion-angry-to" /></div>
                        </div>                            
                    </div>    
                </div>   

                <div class="row">                    
                    <div class="two columns"><h5>Sad</h5></div>
                    <div class="six columns">
                        <br />
                        <div class="row">
                            <div class="two columns">0</div>
                            <div id="sad-slider-range" class="eight columns"></div>
                            <div class="two columns">10</div>
                        </div>                            
                    </div>
                    <div class="four columns row">     
                        <div class="row">
                            <div class="six columns"><input type="text" readonly="readonly" id="emotion-sad-from" name="emotion-sad-from" /></div> 
                            <div class="six columns"><input type="text" readonly="readonly" id="emotion-sad-to" name="emotion-sad-to" /></div>
                        </div>                            
                    </div>    
                </div>   

                <div class="row">                    
                    <div class="two columns"><h5>Peaceful</h5></div>
                    <div class="six columns">
                        <br />
                        <div class="row">
                            <div class="two columns">0</div>
                            <div id="peaceful-slider-range" class="eight columns"></div>
                            <div class="two columns">10</div>
                        </div>                            
                    </div>
                    <div class="four columns row">     
                        <div class="row">
                            <div class="six columns"><input type="text" readonly="readonly" id="emotion-peaceful-from" name="emotion-peaceful-from" /></div> 
                            <div class="six columns"><input type="text" readonly="readonly" id="emotion-peaceful-to" name="emotion-peaceful-to" /></div>
                        </div>                            
                    </div>    
                </div>   

                <div class="row">                    
                    <div class="two columns"><h5>Happy</h5></div>
                    <div class="six columns">
                        <br />
                        <div class="row">
                            <div class="two columns">0</div>
                            <div id="happy-slider-range" class="eight columns"></div>
                            <div class="two columns">10</div>
                        </div>                            
                    </div>
                    <div class="four columns row">     
                        <div class="row">
                            <div class="six columns"><input type="text" readonly="readonly" id="emotion-happy-from" name="emotion-happy-from" /></div> 
                            <div class="six columns"><input type="text" readonly="readonly" id="emotion-happy-to" name="emotion-happy-to" /></div>
                        </div>                            
                    </div>    
                </div>   

                <br /><br /><br/>

                <div class="row">                    
                    <div class="two columns"></div>
                    <div class="four columns">
                        <input type="submit" id="form-action" name="form-action" value="Save" style="width:100%" />
                    </div>
                    <div class="four columns">
                        <input type="submit" id="form-action" name="form-action" value="Logout" style="width:100%" />
                    </div>
                    <div class="two columns">     
                    </div>    
                </div>   

                <div class="row">

                </div>            

            </div>

            <div class="five columns">
                <select id="music-clip" name="music-clip" size="30">
                    <option value="Clip1.wav">Clip 1</option>
                    <option value="Clip2.wav">Clip 2</option>
                    <option value="Clip3.mp3">Clip 3</option>
                    <option value="Clip4.wav">Clip 4</option>
                    <option value="Clip5.wav">Clip 5</option>
                    <option value="Clip6.wav">Clip 6</option>
                    <option value="Clip7.mp3">Clip 7</option>
                    <option value="Clip8.mp3">Clip 8</option>
                    <option value="Clip9.m4a">Clip 9</option>
                </select>
            </div>
        </div>       

        </form>         

	</body>
</html>
